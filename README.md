# QuikLog
A simple logging application written in C++.
Only 1 header to include!

## Features:
* 5 log levels in 5 different colors.
* Includes timestamp & caller function indications by default (no extra code needed).
* Logging to file is supported. Use the `REGISTER_LOG_FILE(...)` macro.

>Note that `std::ofstream` creates files if they don't exist, but not directories.

## How to use:
* Include the 2 files into your project or add a new **Static Library** project and link against it.
* Use the respective `LOG_*` macros exactly like `printf`!

## Log format example:
![image](https://i.imgur.com/cGc7a4S.png)
